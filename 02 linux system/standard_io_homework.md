Note that where `name_lastname` is specified, you must enter your real first and last name, using `_` as a separator. 

### Step 1

- create text file called `input.txt` with some text

- use the `cat` command to display the contents of a text file named `input.txt` to the standard output.

- implement the `echo` command to input a custom message `name_lastname` into a file named `user_input.txt` using the standard input redirection.

- read that `user_input.txt` file



### Step 2

- utilize the `ls` command to list the contents of your home directory and redirect the output to a file named `directory_listing.txt`

- display the current date and time using the `date` command and append it to the `directory_listing.txt` file.



### Step 3

- attempt to remove a non-existent file named `nonexistent_file.txt` using the `rm` command. Redirect the standard error output to a file named `error_log.txt`

- use the `ls` command to list the contents of a directory where you do not have permission, and redirect the standard error output to the `error_log.txt` file.

- read both files

- use the `ls` command to list the contents of a directory that not exists. redirect standard error output to not-existing device(null device that discards any data that is written to it)

Save whole console output in a text file `io_commands.txt` and send it to us.
