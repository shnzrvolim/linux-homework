Note that where `name_lastname` is specified, you must enter your real first and last name, using `_` as a separator. 

### Step 1

- start browser firefox

- use the `ps` command to list all the running processes for all users.

- redirect the output of your `ps` command to a file named `processes_info.txt`

- filter and show only the PID (Process ID) of a process firefox

### Step 2

- use the neccecery command to terminate the identified process of firefox by its PID.

- verify that the process has been terminated successfully using.

### Step 3

- start browser firefox

- identify a process by name that you would like to terminate

- use the neccecery command to terminate all instances of the identified process by name.

- confirm the termination by using `ps` or another appropriate command.

### Step 4

- use `ps` command with parameters to filter processes using highest memory

- create alias to this command called `my_name_lastname_procces_alias`

- show info using alias

- redirect the output of your command to a file named `processes_info_alias.txt`

- make this alias permanent

Save whole console output in a text file `processes_commands.txt` and send it to us.
