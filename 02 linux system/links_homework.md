Note that where `name_lastname` is specified, you must enter your real first and last name, using `_` as a separator. 

### Step 1

- create directory with path `linux_2/homework1/step1`

- create file `name_lastname.txt` in `linux_2/homework1/step1` directory and fill it with some text

- move into directory and display the contents of the directory, show info about **inode**

- move to your home directory

- create `symbolic link` to the `name_lastname.txt` with the name `test1`

- display the contents of the current directory, show info about **inode**

### Step 2

- using `test1` add text the file

- read the first 3 lines of the file using the `test1` link

- delete `linux_2\homework1\step1\name_lastname.txt` file

- try to read the first 3 lines of the file using the `test1` link

### Step 3

- create a new directory called your `name_lastname`

- create `symbolic link` named `path1` to  `name_lastname` directory

- show info about **inode**

- create 3 files l1.txt, l2.txt, l3.txt

- using `path1` move that files into `name_lastname` directory

- change name of your directory to `lastname_name`

- try list using `path1` link

### Step 4

- Inside the`lastname_name` directory create text file named `text.txt` and fill some text

- show info about **inode**

- now navigate back to your user home directory

- create `hard link` to `text.txt` named `my_hard_link`

- show info about **inode**

- read last 5 line of file using `my_hard_link`

### Step 5

- now delete `text.txt` file

- try to read last 5 line of file using `my_hard_link`

- show info about **inode**

- remove `my_hard_link` to permanently delete the file

Save whole console output in a text file `links_commands.txt` and send it to us.
