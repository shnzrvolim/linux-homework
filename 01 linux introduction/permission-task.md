Note that where `name_lastname` is specified, you must enter your real first and last name, using `_` as a separator.

### Step 1

- create a subdirectories named `permission_task/name_lastname`

- navigate into the `permission_task/name_lastname` directory

### Step 2

- create a new text file named `permissions_name_lastname.txt` and write with in `Hello name_lastname 123`

- determine the type of file `permissions_name_lastname.txt` and show in console

### Step 3

- display detailed information about `permissions_name_lastname.txt` file permissions

- modify the permissions of `permissions_name_lastname.txt` to remove write permission for the group and others using: `symbolic mode`

- display detailed information about `permissions_name_lastname.txt` file permissions

- modify the permissions of `permissions_name_lastname.txt` to ensure only the owner can read and write to the file using: `octal(numeric) mode`

- display detailed information about `permissions_name_lastname.txt` file permissions

### Step 4

- create a subdirectories `p1` `p2` `p3` in `permission_task/name_lastname`

- copy `permissions_name_lastname.txt` file in to all `p1` `p2` `p3` directories

- apply the `-rwxrw-r--` permission change recursively to all files within created  `p1` `p2` `p3` directories 



Save whole console output in a text file `permission_task_commands.txt` and send it to us.


