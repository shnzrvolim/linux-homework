Note that where `name_lastname` is specified, you must enter your real first and last name, using `_` as a separator. 

### Step 1

- show info about ubuntu version

### Step 2

- print the present working directory

- display the contents of the current directory, show hidden folders and all contents in detail.

### Step 3

- create a new directory named your `name_lastname`

- navigate into the `name_lastname` directory

- print the present working directory

### Step 4

- Inside the `name_lastname` directory:
  
  - create a subdirectories named `report/linux_day_1/folder_1` using single command

- now navigate back to your user home directory

- create `folder_2`, `folder_3` using single command

- move both folders into `report/day` directory using single command

### Step 5

- now navigate back to your user home directory

- remove your `name_lastname` directory using single command



Save whole console output in a text file `directory_navigation_commands.txt` and send it to us.
