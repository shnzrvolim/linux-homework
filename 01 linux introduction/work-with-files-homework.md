Note that where `name_lastname` is specified, you must enter your real first and last name, using `_` as a separator.

### Step 1

- print the present working directory

- display the contents of the current directory, show hidden folders and all contents in detail.

### Step 2

- create a subdirectories named  `work_with_files/name_lastname` in single command

- navigate into the `work_with_files/name_lastname` directory

- print the present working directory

### Step 3

- create new file named `linux_day_1.txt`

- create a copy of `linux_day_1.txt` called `linux_day_1_backup.txt`

- show list of files

### Step 4

- rename `linux_day_1.txt` to `linux_day_2.txt`

- sorts files by modification time and show list of files

- now navigate back to your user home `work_with_files` directory

- being in a `work_with_files` directory show only show only content of `name_lastname` directory

### Step 5

- show list of folders with only name and creation date

- create copy of `work_with_files` directory with name `work_with_files_backup`

- remove `work_with_files` directory with all its subdirectory and files

- rename `work_with_files_backup` into `work_with_files`



Save whole console output in a text file `work_with_files_commands.txt` and send it to us.
