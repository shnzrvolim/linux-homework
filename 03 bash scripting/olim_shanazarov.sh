echo $USER
echo $0 | cut -d "." -f 2 | tr -d / | tr _ " "

if [ -e /var/log ]; then
    echo "Directory exists!"
else 
    echo "No such directory"
fi

for file in "/var/log/*.log"; 
do
    cp -v $file /tmp
done    

find /tmp -type f -mmin -3