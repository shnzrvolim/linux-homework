Note that where `name_lastname` is specified, you must enter your real first and last name, using `_` as a separator. 

### Step 1

- create folder in your home directory called `name_surname`

- use the `find` command to locate all text files (files with a `.txt` or `.TXT`  using expresion) in the `/etc` directory and its subdirectories

- display the full path and filename for each text file found

### Step 2

- use pipes or include functions of `find` to redirect result into list command and show all info about founded files.

### Step 3

- use `pipes` and `grep` command to search in `/etc/passwd` file all lines that containing the word `root` display the line number

### Step 4

- create a new directory named `name_lastname` within the `/home/user/documents` directory
- find all files modified within the last 7 days in the `/var/log` directory and copy them to the `name_lastname` directory

### Step 5

- list all files in the `/etc` directory that have the word `network` in them

- use a pipe to send this output to `grep` and filter for lines containing the word `conf.`

- display only the line numbers and matching lines.

Save whole console output in a text file `pipes_commands.txt` and send it to us.
