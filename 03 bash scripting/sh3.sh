if [ $# -eq 0 ]; then
    echo "No parameter provided"
    exit 0
elif [ $# -eq 1 ]; then
    echo "Single parameter provided: $1"
    exit 0
fi

sum=0

echo "Listing provided parameters:"
for param in $@;
do  
    echo $param
    ((sum+=$param))
done

echo -e "\nSum of all numbers in parameters: $sum"