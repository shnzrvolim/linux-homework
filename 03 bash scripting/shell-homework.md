Note that where `name_lastname` is specified, you must enter your real first and last name, using `_` as a separator.

### Step 1

create a directory `scrits` 

navigate into `scripts` directory

### Step 2

- create a shell file inside and name it your `name_lastname.sh`

- add to your user run permission to the script

### Script 1

- script must print your user and your `name lastname` from specific variables

- check if the `/var/log` directory exists using the if statment

- using for loop, copy all files ending with `.log` from the `/var/log/` folder to the `/tmp` directory

- show all files that added in `/tmp` directory in last 3 min

### Script 2

- create script called `sh2.sh`

- write a while loop that will create 4 files ex1..4 in /home/your_user/Desktop directory.

- copy these files to the `/home/your_user/scripts/copy_task/` directory

- check if you have permission to run the ex1 file

- rename the ex2 file to log

### Script 3

- create script called `sh3.sh`

- script must accept a single command-line parameter

- If a parameter is provided, print a message indicating that the script received a parameter and display the provided value

- else no parameter is provided, print a message indicating that no parameter was given

- extend the script to handle multiple parameters

- use a loop to iterate through all the provided parameters and print each one on a new line

- if the provided parameters contain numbers, sum these numbers and after the loop completes, print the sum

### Script 4

- create script called `sh4.sh`

- script must combine two files content in one

- pass to script two files path If a parameters not provided, print a error message 

- create finction `combine_files` that returns two files content

- create new file and write returned content in it

- show info about created file and read it

Execute scripts and show results save whole console output in a text file `scripts_commands.txt` and also send to us `name_lastname.sh`, `sh2.sh`, `sh3.sh`
