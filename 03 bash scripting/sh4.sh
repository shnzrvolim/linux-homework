if [ $# -ne 2 ]; then
    echo "Please provide 2 files"
    exit 1
fi

function combine_files() {
    echo "$(cat "$1")"
    echo "============"
    echo "$(cat "$2")"
}

echo "$(combine_files $1 $2)" > new_combined_file
echo "$(ls -l new_combined_file)"
cat new_combined_file